package com.nexsoft.formative17.controllers;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.nexsoft.formative17.models.dao.AddressDaoImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    @Autowired
    AddressDaoImpl addressDaoImpl;

    @GetMapping("/")
    public String welcome(Model model) {

        return "Home";
    }

    @PostMapping("/execute")
    public String execute(String query, Model model) {
        String key = getKey(query).toUpperCase();

        if (key.equals("INSERT") || key.equals("CREATE") || key.equals("UPDATE") || key.equals("DELETE")) {
            addressDaoImpl.addAddress(query);
        }

        if (key.equals("SELECT")) {
            // List<Address> address = addressDaoImpl.getAllAddress(query);

            ArrayList<LinkedHashMap<String, Object>> listAddress = addressDaoImpl.getAllAddress(query);

            
            model.addAttribute("listAddress", listAddress);
        }

        return "Home";
    }

    public String getKey(String query) {
        String[] words = query.split("\\s+");
        for (int i = 0; i < words.length; i++) {

            words[i] = words[i].replaceAll("[^\\w]", "");
        }

        return words[0];

    }
}
