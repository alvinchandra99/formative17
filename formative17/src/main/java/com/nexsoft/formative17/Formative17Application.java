package com.nexsoft.formative17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative17Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative17Application.class, args);
	}

}
