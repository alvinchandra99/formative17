package com.nexsoft.formative17.models.dao;

import java.util.ArrayList;

import java.util.LinkedHashMap;

public interface AddressDao {

    public void addAddress(String query);

    public ArrayList<LinkedHashMap<String, Object>> getAllAddress(String query);

}
