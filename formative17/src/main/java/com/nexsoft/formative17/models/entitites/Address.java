package com.nexsoft.formative17.models.entitites;

public class Address {

    private int id;

    private String street;

    private String city;

    private String country;

    private int zipCode;

    public Address() {
    }

    public Address(int id, String street, String city, String country, int zipCode) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.country = country;
        this.zipCode = zipCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }
    
}
