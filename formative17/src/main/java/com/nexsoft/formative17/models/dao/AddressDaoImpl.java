package com.nexsoft.formative17.models.dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class AddressDaoImpl implements AddressDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void addAddress(String query) {
        jdbcTemplate.execute(query);
    }

    @Override
    public ArrayList<LinkedHashMap<String,Object>> getAllAddress(String query) {

        try {

            Connection con = DataSourceUtils.getConnection(jdbcTemplate.getDataSource()); // your datasource
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query); // your query
       

       

            ArrayList<LinkedHashMap<String,Object>> arr = new ArrayList<>();

            while (rs.next()) {

                int columns = rs.getMetaData().getColumnCount();
                LinkedHashMap<String, Object> obj = new LinkedHashMap<String, Object>();

                for (int i = 0; i < columns; i++)
                    obj.put(rs.getMetaData().getColumnLabel(i + 1).toLowerCase(), rs.getObject(i + 1));

                arr.add(obj);
            }

            return arr;
            


        } catch (SQLException s) {

        }

        return new ArrayList<LinkedHashMap<String,Object>>();

    }

}
