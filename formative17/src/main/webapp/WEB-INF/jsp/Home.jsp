<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglibprefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

  <form action="/execute" method="post">
  <div class="container mt-5" style="max-width:800px">
    <div class="mb-3">
        <label for="exampleFormControlTextarea1" class="form-label">Query</label>
        <textarea class="form-control" name="query" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Execute</button>

    </div>
    </form>

    <div class="container mt-5" style="max-width:800px">
        <c:if test="${not empty listAddress}" >
        <table class="table">
            <thead>
              <tr>
                <c:forEach items="${listAddress.get(0)}" var="a">
                <th scope="col">${a.key}</th>
              </c:forEach>

              </tr>
            </thead>
            <tbody>
        
                <c:forEach items="${listAddress}" var="x">
                  <tr scope="row">
                  <c:forEach items="${x}" var="a">
               
                 <td>${a.value}</td> 
              </c:forEach>
            </tr>
            </c:forEach>
             
        
            </tbody>
          </table>

   

    </c:if>
    </div>
    
    
    




    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>